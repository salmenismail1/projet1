const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { Hotel } = require('../Models/hotel');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.post("/api/AjoutHotel",(req,res,next) => {
  const h = new Hotel({

      id :  req.body.id,
      nom : req.body.nom,
      adultOnly : req.body.adultOnly,
      ville : req.body.ville,
      categorie : req.body.categorie,
      type : req.body.type,
      lpdvente : req.body.lpdvente,
      dpvente : req.body.dpvente,
      pcvente : req.body.pcvente,
      allinsoftvente : req.body.allinsoftvente,
      allinvente : req.body.allinvente,
      ultraallinvente : req.body.ultraallinvente,
      age_enf_gratuit : req.body.age_enf_gratuit,
      image:req.body.image

  });
  h.save();
  res.status(201).json({
      message:"L'ajout de l'hotel avec succée"
  });
});


app.get('/api/ListerHotel', (req, res) => {

  Hotel.find().then((Hotel) => {
      if (Hotel) {
          res.status(200).send(Hotel);
      }
      else { console.log("not found" + err.message) }

  })

});

app.delete("/api/Hotel/:nom", (req, res) => {

  Hotel.findOneAndRemove(
      {
          nom: req.params.nom,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
              console.log(doc);
          }
          else { console.log('Error in Hotel Delete :' + err) }
      });
});









module.exports = app;