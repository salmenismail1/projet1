const express = require('express');
const bodyParser = require('body-parser');

const { RVoyage }  = require('../Models/ReservationVoyage');

var app = express();
app.use(bodyParser.json());


app.post("/api/reservation",(req,res,next) => {
  const r = new RVoyage({

      
      Nom: req.body.Nom,
      Email: req.body.Email,
      Date:  req.body.Date

  });
  r.save();
  res.status(201).json({
      message:"L'ajout du reservation de voyage avec succée"
  });
});



app.delete("/api/reservation/:nom", (req, res) => {

  RVoyage.findOneAndRemove(
      {
          Nom: req.params.nom,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
              console.log(doc);
          }
          else { console.log('Error in Reservation Voyage  Delete :' + err) }
      });
});

app.get('/api/ReservationVoyage', (req, res) => {

  RVoyage.find().then((RVoyage) => {
      if (RVoyage) {
          res.status(200).send(RVoyage);
      }
      else { console.log("not found" + err.message) }

  })

});

module.exports = app;