const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { Hotel } = require('../Models/hotel');
const { RHotel } = require('../Models/ReservationHotel');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.post("/api/AjoutHotel",(req,res,next) => {
  const h = new Hotel({

      id :  req.body.id,
      nom : req.body.nom,
      adultOnly : req.body.adultOnly,
      ville : req.body.ville,
      categorie : req.body.categorie,
      type : req.body.type,
      lpdvente : req.body.lpdvente,
      dpvente : req.body.dpvente,
      pcvente : req.body.pcvente,
      allinsoftvente : req.body.allinsoftvente,
      allinvente : req.body.allinvente,
      ultraallinvente : req.body.ultraallinvente,
      age_enf_gratuit : req.body.age_enf_gratuit,
      image:req.body.image

  });
  h.save();
  res.status(201).json({
      message:"L'ajout de l'hotel avec succée"
  });
});


app.get('/api/ListerHotel', (req, res) => {

  Hotel.find().then((Hotel) => {
      if (Hotel) {
          res.status(200).send(Hotel);
      }
      else { console.log("not found" + err.message) }

  })

});

app.delete("/api/Hotel/:nom", (req, res) => {

  Hotel.findOneAndRemove(
      {
          nom: req.params.nom,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
              console.log(doc);
          }
          else { console.log('Error in Hotel Delete :' + err) }
      });
});


app.put("/HotelModif/:id", (req, res) => {
    
    var hotel = {
        id :  req.body.id,
        nom : req.body.nom,
        adultOnly : req.body.adultOnly,
        ville : req.body.ville,
        categorie : req.body.categorie,
        type : req.body.type,
        lpdvente : req.body.lpdvente,
        dpvente : req.body.dpvente,
        pcvente : req.body.pcvente,
        allinsoftvente : req.body.allinsoftvente,
        allinvente : req.body.allinvente,
        ultraallinvente : req.body.ultraallinvente,
        age_enf_gratuit : req.body.age_enf_gratuit,
        image:req.body.image

    };

    Hotel.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: hotel }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});

//******************** Reservation Hotel ********************* */

app.post("/api/reservation",(req,res) => {
    const r = new RHotel({

        Nom: req.body.Nom,
        Email: req.body.Email,
        Date:  req.body.Date
  
    });
    r.save();
    res.status(201).json({
        message:"L'ajout du reservation de Hotel avec succée"
    });
  });
  
  
  
  app.delete("/api/DeleteReservation/:nom", (req, res) => {
  
    RHotel.findOneAndRemove(
        {
            Nom: req.params.nom,
        },
  
        (err, doc) => {
            if (!err) {
                res.status(200).send(doc);
                console.log(doc);
            }
            else { console.log('Error in Reservation Hotel  Delete :' + err) }
        });
  });
  
  app.get('/api/ReservationHotel', (req, res) => {
  
    RHotel.find().then((ReservationHotel) => {
        if (ReservationHotel) {
            res.status(200).send(ReservationHotel);
        }
        else { console.log("not found" + err.message) }
  
    })
  
  });


  app.put("/ReservationHotelModif/:id", (req, res) => {
    
    var resHotel = {
        Nom: req.body.Nom,
        Email: req.body.Email,
        Date:  req.body.Date

    };

    RHotel.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: resHotel }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});

module.exports = app;