const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { RCircuit } = require('../Models/ReservationCircuit');
const { Circuit } = require('../Models/Circuit');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.post("/api/AjoutCircuit",(req,res,next) => {
  const circuit = new Circuit({

      id :  req.body.id,
      nom : req.body.nom,
      image : req.body.image

  });
  circuit.save();
  res.status(201).json({
      message:"L'ajout de Circuit avec succée"
  });
});


app.get('/api/ListerCircuit', (req, res) => {

    Circuit.find().then((Circuit) => {
      if (Circuit) {
          res.status(200).send(Circuit);
      }
      else { console.log("not found" + err.message) }

  })

});

app.delete("/api/DeleteCircuit/:nom", (req, res) => {

    Circuit.findOneAndRemove(
      {
          nom: req.params.nom,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
              console.log(doc);
          }
          else { console.log('Error in Circuit Delete :' + err) }
      });
});


app.put("/CircuitModif/:id", (req, res) => {
    
    var circuit = {
        id :  req.body.id,
        nom : req.body.nom,
        image : req.body.image

    };

    Circuit.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: circuit }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});

//******************** Reservation Circuit ********************* */

app.post("/api/reservationCircuit",(req,res) => {
    const r = new RCircuit({

        Nom: req.body.Nom,
        Email: req.body.Email,
        Date:  req.body.Date
  
    });
    r.save();
    res.status(201).json({
        message:"L'ajout du reservation de Hotel avec succée"
    });
  });
  
  
  
  app.delete("/api/DeleteReservationCircuit/:nom", (req, res) => {
  
    RCircuit.findOneAndRemove(
        {
            Nom: req.params.nom,
        },
  
        (err, doc) => {
            if (!err) {
                res.status(200).send(doc);
                console.log(doc);
            }
            else { console.log('Error in Reservation Circuit  Delete :' + err) }
        });
  });
  
  app.get('/api/ListerReservationCircuit', (req, res) => {
  
    RCircuit.find().then((ReservationCircuit) => {
        if (ReservationCircuit) {
            res.status(200).send(ReservationCircuit);
        }
        else { console.log("not found" + err.message) }
  
    })
  
  });


  app.put("/ReservationCircuitModif/:id", (req, res) => {
    
    var resCircuit = {
        Nom: req.body.Nom,
        Email: req.body.Email,
        Date:  req.body.Date

    };

    RCircuit.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: resCircuit }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});







module.exports = app;