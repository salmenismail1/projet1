const mongooose = require('mongoose');


const Voyageschema = new mongooose.Schema(
    {
        
        id: {
            type: String,
            require: true

        },
        nom: {
            type: String,
            require: true
        },
        pays: {
            type: String,
            require: true
        },
        titre: {
            type: String,
            require: true
        },
        prix: {
            type: String,
            require: true
        },
        nbrejour: {
            type: String,
            require: true
        },
        nbrenuitee: {
            type: String,
            require: true
        },
        departvoyages: [
            {
                datedepart:String,
                dateretour:string
            }
        ],
        imageVoyages:[
            {
                id: String,
                nom: String
            }
        ]
        
    }

);

Voyage = mongooose.model('Voyage', Voyageschema);
module.exports = { Voyage };



