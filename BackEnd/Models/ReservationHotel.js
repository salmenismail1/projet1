const mongooose = require('mongoose');


const ReservationHotelschema = new mongooose.Schema(
    {
        
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Date: {
            type: String,
            require: true,
            trim: true
        },
    }

);

RHotel = mongooose.model('ReservationHotel', ReservationHotelschema);
module.exports = { RHotel };
