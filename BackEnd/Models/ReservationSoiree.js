const mongooose = require('mongoose');


const ReservationSoireeschema = new mongooose.Schema(
    {
        
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Date: {
            type: String,
            require: true,
            trim: true
        },
    }

);

ReservationSoiree = mongooose.model('ReservationSoiree', ReservationSoireeschema);
module.exports = { ReservationSoiree };
