const mongooose = require('mongoose');


const ReservationOmraschema = new mongooose.Schema(
    {
        
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Date: {
            type: String,
            require: true,
            trim: true
        },
    }

);

ReservationOmra = mongooose.model('ReservationOmra', ReservationOmraschema);
module.exports = { ReservationOmra };
