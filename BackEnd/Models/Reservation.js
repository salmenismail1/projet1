const mongooose = require('mongoose');


const Reservationschema = new mongooose.Schema(
    {
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Date: {
            type: String,
            require: true,
            trim: true
        },
        
    }

);

Reservation = mongooose.model('Reservation', Reservationschema);
module.exports = { Reservation };
