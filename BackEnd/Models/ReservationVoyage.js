const mongooose = require('mongoose');

const ReservationVoyage = new mongooose.Schema(
    {
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Date: {
            type: String,
            require: true,
            trim: true
        },
    }

);


RVoyage = mongooose.model('ReservationVoyage', ReservationVoyage);
module.exports = { RVoyage };