const mongooose = require('mongoose');


const ReservationCircuitschema = new mongooose.Schema(
    {
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Date: {
            type: String,
            require: true,
            trim: true
        },
        
    }

);

ReservationCircuit = mongooose.model('ReservationCircuit', ReservationCircuitschema);
module.exports = { ReservationCircuit };
