import { Component, OnInit } from '@angular/core';
import { CircuitService } from 'src/app/Services/circuit.service';
import { Circuit } from 'src/app/Models/circuit';


@Component({
  selector: 'app-circuit',
  templateUrl: './circuit.component.html',
  styleUrls: ['./circuit.component.css']
})
export class CircuitComponent implements OnInit {
  circuit = new Circuit;
  ListeCircuits;

  constructor(private circuitServices : CircuitService) { }

  ngOnInit() {
    this.circuitServices.getCircuit(this.circuit).subscribe(        
      response =>{
        this.ListeCircuits=response;
        console.log(response)
      },
      err =>{"error"}
    )
  }

}
