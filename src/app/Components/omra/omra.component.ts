import { Component, OnInit } from '@angular/core';
import { OmraService } from 'src/app/Services/omra.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-omra',
  templateUrl: './omra.component.html',
  styleUrls: ['./omra.component.css']
})
export class OmraComponent implements OnInit {
  ListeOmra;

  constructor(private omraService:OmraService) { }

  ngOnInit() {
    this.omraService.getOmra().subscribe(        
      response =>{
        this.ListeOmra=response;
        console.log(response.id)
      },
      err =>{"error"}
    )
    

  }
  

}
