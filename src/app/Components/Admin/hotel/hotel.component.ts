import { Component, OnInit } from '@angular/core';
import { HotelsService } from 'src/app/Services/hotels.service';


@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {

  ListHotel;

  constructor(private hotelService :HotelsService) { }

  ngOnInit() {

    this.hotelService.getHotel2().subscribe(        
      response =>{
        this.ListHotel=response;
        console.log(response)
      },
      err =>{"error"}
    )

  }
  delete(nom:string){
    this.hotelService.deleteHotel(nom).subscribe(
      response =>{
        console.log(response)
        this.ngOnInit();
      }
    )
  }

}
