import { Component, OnInit, Input, Sanitizer, ViewChild } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { Caroussel } from 'src/app/Models/caroussel';
import { Observable, Subject, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HotelsService } from 'src/app/Services/hotels.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { VoyagesService } from 'src/app/Services/voyages.service';
import { Voyages } from 'src/app/Models/voyages';
import { User } from 'src/app/Models/user';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;

backgroundUrl="http://www.wa11papers.com/assets/previews/nature-sea-landscape-ocean-sunset-sunrise-wallpaper-1082-preview-0e49f9f0.jpg";
url2;
ListeHotels;
num=0;
ListeVoyages;
voyage = new Voyages;

l: User[] = [];
  private postsSub: Subscription;
  constructor(private sanitization:DomSanitizer,private httpClient:HttpClient,private hotelServices:HotelsService,private voyageService:VoyagesService) { 

  }
  ngOnInit() {



     
    


    this.voyageService.getVoyage(this.voyage).subscribe(        
      response =>{
        this.ListeVoyages=response;
        

        console.log(response[0])
        console.log(this.l)
      },
      err =>{"error"}
    )

    
    this.hotelServices.getHotel().subscribe(        
      response =>{
        this.ListeHotels=response;
        console.log(response)
      },
      err =>{"error"}
    )

  }

  prev(){
    if(this.num==0){
      this.num=4;

    }else{
      this.num--;
    }

  }
  
  next(){
if(this.num==4){
  this.num=0;

}else{
  this.num++;

}

  }

  /*<ngb-carousel #ngcarousel style="width: 100%;" class="d-flex flex-wrap justify-content-center">


        <div *ngFor="let item of ListeHotels ; let i = index" style="width: 33%;"  > 
          <ng-template ngbSlide >

            <div class="card" style="width: 18rem;" *ngIf="i < (5)">
                <img class="card-img-top" src="https://freedomtravel.tn/assets/images/hotel_miniature/{{item.id}}.jpg" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>    
            </ng-template>

            </div>
      
</ngb-carousel>*/
 

}