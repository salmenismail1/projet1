import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { ModalDirective } from 'angular-bootstrap-md';
import { MatDialogRef } from '@angular/material/dialog';
import { LoginService } from 'src/app/Services/login.service';
import { Login } from 'src/app/Models/login';
import { User } from 'src/app/Models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  log = new Login  ;
  user = new User;

  constructor(public dialogRef: MatDialogRef<LoginComponent>,private loginserv:LoginService) { }

  ngOnInit(){}
  
  actionFunction() {
    alert("You have logged out.");
    this.closeModal();
  }

  closeModal() {
    this.dialogRef.close();
 
  }

  login(){
    console.log(this.log);
    this.loginserv.postLogin(this.log).subscribe(
      
      response =>{
            localStorage.setItem('token',response.token);
            localStorage.setItem('refresh_token',response.refresh_token);
            console.log(response)
            this.dialogRef.close();
      },
      err =>{"error"}
    )
  }
  inscription(){
    console.log(this.user);
    this.loginserv.postInscrit(this.user).subscribe(
      
      response =>{
            console.log(response)
            this.dialogRef.close();
      },
      err =>{"error"}
    )
  }
  
}