import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HreservationComponent } from './hreservation.component';

describe('HreservationComponent', () => {
  let component: HreservationComponent;
  let fixture: ComponentFixture<HreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
