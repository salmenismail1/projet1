import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HotelsService } from 'src/app/Services/hotels.service';
import {formatDate } from '@angular/common';
import { NgbModule, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { ReservationHotel } from 'src/app/Models/reservation-hotel';
import { ViewChild } from '@angular/core';
import { Hotels } from 'src/app/Models/hotels';
import { Observable } from 'rxjs';
import { Caroussel } from 'src/app/Models/caroussel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
const  url ="https://freedomtravel.tn/json/carouselHotel.php?id=";
const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};

@Component({
  selector: 'app-hreservation',
  templateUrl: './hreservation.component.html',
  styleUrls: ['./hreservation.component.css']
})

export class HreservationComponent implements OnInit {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;

  ReservationHotel = new ReservationHotel;
  
id;
obj;
nbAdult=1;
nbEnfant=0;

d=new Date();
ListeHotels;
Listimg;
listAdult:number[] = [1, 2, 3,4];
listEnf: number[] = [1, 2, 3,4];

  constructor(private Activate:ActivatedRoute,private service:HotelsService,private httpClient:HttpClient) { }

  ngOnInit() {
    this.id=this.Activate.snapshot.paramMap.get("id");

this.obj=formatDate(new Date(), 'yyyy/MM/dd', 'en');

this.service.getHotel().subscribe(        
  response =>{
    this.ListeHotels=response;
    console.log(response)
  },
  err =>{"error"}
)
this.service.getHotel().subscribe(        
  response =>{
    this.ListeHotels=response;
    console.log(response)
  },
  err =>{"error"}
)
console.log(this.ListeHotels);
this.getImg().subscribe(        
  response =>{
    this.Listimg=response;
    console.log(response)
  },
  err =>{"error"}
)





  }


  getImg():Observable<Caroussel>{
    
    return this.httpClient.get<Caroussel>(url+this.id,httpOptions);
  }
  verif(){

    this.listAdult=[];
    for(let i=0;i<(4-this.nbEnfant);i++){
this.listAdult.push(i+1);
    }
    this.listEnf=[];

    for(let i=0;i<(4-this.nbAdult);i++){
this.listEnf.push(i+1);
    }
    console.log(this.listAdult);
    
  }

  AjouterReservation(){
    console.log(this.ReservationHotel);
    this.service.postContact(this.ReservationHotel).subscribe(
      response =>{
        console.log(response)
      },
      err =>{"error"}
    )
  }
  
}