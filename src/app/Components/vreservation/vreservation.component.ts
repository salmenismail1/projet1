import { Component, OnInit,  AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import {HttpHeaders, HttpClient } from '@angular/common/http';
import { Voyages } from 'src/app/Models/voyages';
import { Observable } from 'rxjs';


const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};

@Component({
  selector: 'app-vreservation',
  templateUrl: './vreservation.component.html',
  styleUrls: ['./vreservation.component.css']
})
export class VreservationComponent implements OnInit,AfterViewInit {

  nbChambre = new Array(5);
  id;
  unvoyage;
  voyage;
  x;
  nb;
  nb2;
  constructor(private Activate:ActivatedRoute,private httpClient:HttpClient) { }
 
  ngOnInit() {
    
    this.nb=4;
    this.id =this.Activate.snapshot.paramMap.get("id");
    this.unvoyage = "https://topresa.ovh/api/departvoyages.json?departvoyages.datedepart[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en')+"&voyage.id="+this.id;
    console.log(this.unvoyage);

    this.getUnVoyage().subscribe(        
      response =>{
        this.x=response[0];
        this.voyage=response;
        console.log(this.x)
        console.log(response)
      }
    )
  }
  getUnVoyage():Observable<Voyages>{
    return this.httpClient.get<Voyages>(this.unvoyage,httpOptions);

  }
  chambre(){
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);

      if(selectElement.hidden==true){
        selectElement.hidden=false;
        return;
      }
    }
  }
  delchambre(i:number){
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
    selectElement.hidden=true;

    const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
    select1.selectedIndex=0;

    const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
    select2.selectedIndex=0;

    const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
    select3.selectedIndex=0;

    const inputprix = <HTMLSelectElement>document.getElementById('prix'+i);
    inputprix.value="";

  }
  ngAfterViewInit(): void {
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+0);
    selectElement.hidden=false;


  }
}

