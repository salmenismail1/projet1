import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VreservationComponent } from './vreservation.component';

describe('VreservationComponent', () => {
  let component: VreservationComponent;
  let fixture: ComponentFixture<VreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
