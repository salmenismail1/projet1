import { Component, OnInit } from '@angular/core';
import { VoyagesService } from 'src/app/Services/voyages.service';
import { Voyages } from 'src/app/Models/voyages';

@Component({
  selector: 'app-voyage',
  templateUrl: './voyage.component.html',
  styleUrls: ['./voyage.component.css']
})
export class VoyageComponent implements OnInit {
  voyage = new Voyages;
  ListeVoyages;

  constructor(private voyageService:VoyagesService) { }

  ngOnInit() {
    this.voyageService.getVoyage(this.voyage).subscribe(        
      response =>{
        this.ListeVoyages=response;
        console.log(response)
      },
      err =>{"error"}
    )
  }

}
