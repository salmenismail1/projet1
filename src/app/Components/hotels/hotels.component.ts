import { Component, OnInit } from '@angular/core';
import { HotelsService } from 'src/app/Services/hotels.service';
import { Hotels } from 'src/app/Models/hotels';
import { DomSanitizer } from '@angular/platform-browser';
import { FiltersPipe } from 'src/app/filters.pipe';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})

export class HotelsComponent implements OnInit {

  ListeHotels;
  text;
  listVille;
  ville ;
  test="";
  etoile;
  max;
  min;
  
 

  constructor(private hotelServices:HotelsService,private sanitizer:DomSanitizer) { }

  ngOnInit() {
    this.hotelServices.getHotel().subscribe(        
      response =>{
        this.ListeHotels=response;
        console.log(response)
      },
      err =>{"error"}
    )
    this.hotelServices.getVille().subscribe(
      ville =>{
        this.listVille=ville;},
      error => console.log(error) 
    );

  }

  getSanitizerUrl(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);

  }

  getPrix(hotel: Hotels)
  {
    return this.hotelServices.getPrix(hotel);
  }
  testing1(){
    console.log(this.test);
    this.test= ""; 
    this.reset();
  }
  
  testing2(){
    console.log(this.test);
    this.test= "ville";
    this.test= "f"; 
    this.text= ""; 
  
  
  }
  
    testing3(s:string){
      this.etoile=s;
      console.log(this.test);
      this.test= "etoile"; 
      this.test= "f";
      this.text= ""; 
    }
    testing4(){
      this.test="prix";
      console.log("min : "+this.min+"max : "+this.max);
      this.test= "f" ;
          this.text= ""; 
    }
    reset(){
      this.min=0;
      this.max=300;
      this.ville="Liste des Villes";

    }
  
}