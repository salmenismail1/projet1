import { Component, OnInit } from '@angular/core';
import { SoireesService } from 'src/app/Services/soirees.service';

@Component({
  selector: 'app-soiree',
  templateUrl: './soiree.component.html',
  styleUrls: ['./soiree.component.css']
})
export class SoireeComponent implements OnInit {
ListeSoirees;
l;
  constructor(private soireeService :SoireesService) { }

  ngOnInit() {
    this.soireeService.getSoiree().subscribe(        
      response =>{
        this.ListeSoirees=response;

        console.log(response);
      },
      err =>{"error"}
    )
  }

}
