import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { LoginComponent as ModalComponent } from '../login/login.component';
import { LoginService } from 'src/app/Services/login.service';
import { Login } from 'src/app/Models/login';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user = new Login ;

  constructor(public matDialog: MatDialog,private loginserv:LoginService) { }

  taille=100;

  ngOnInit() {

  }
  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    this.matDialog.open(ModalComponent, {
      height: '500px',
      width: '500px',
    });
    
  }
  navFun(page:string){

    if(page=="home"){
      this.taille=100;
    }else{
      this.taille=40;

    }
  }
  loggued(){
    return this.loginserv.loggedIn();
  }

  logOut(){
    
      this.loginserv.logOut()
      console.log(this.user)
      console.log(this.loginserv.loggedIn());
    }

}