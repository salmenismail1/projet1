import { Injectable } from '@angular/core';
import { HttpHeaders ,HttpClient} from '@angular/common/http';
import { Voyages } from '../Models/voyages';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';

const url = "https://topresa.ovh/api/voyages.json?departvoyages.datedepart[after]=2020-02-14 ";
const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};


@Injectable({
  providedIn: 'root'
})
export class VoyagesService {

  constructor(private httpClient:HttpClient) {}

  getVoyage(voyage:Voyages):Observable<Voyages>{
    
    return this.httpClient.get<Voyages>(url,httpOptions);
  }
}
