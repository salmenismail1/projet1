import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Hotels } from '../Models/hotels';
import { Observable, Subject } from 'rxjs';
import { User } from '../Models/user';
import { ReservationHotel } from '../Models/reservation-hotel';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const HotelUrl = "https://freedomtravel.tn/json/hotels2.php";
const AjoutReservationUrl ="http://localhost:3000/hotel/api/reservation"
const  villeUrl = "https://www.freedomtravel.tn/ng/villes.php";

const Hotel2Url = "http://localhost:3000/hotel/api/ListerHotel" ;

const DeleteHotelUrl = "http://localhost:3000/hotel/api/Hotel/";





@Injectable({
  providedIn: 'root'
})
export class HotelsService {
  private posts: User[] = [];
  private postsUpdated = new Subject<User[]>();
  constructor(private httpClient:HttpClient) {}


  getHotel():Observable<Hotels>{
    return this.httpClient.get<Hotels>(HotelUrl,httpOptions);
  }

  getHotel2():Observable<Hotels>{
    return this.httpClient.get<Hotels>(Hotel2Url,httpOptions);
  }


  getVille(){
    return this.httpClient.get(villeUrl);
  }

  getPrix(hotel:Hotels){
    if (hotel.lpdvente !=0)
      return hotel.lpdvente;
    else if(hotel.dpvente)
      return hotel.dpvente;
    else if (hotel.allinvente)
      return hotel.allinvente;
    else (hotel.allinsoftvente)
      return hotel.allinsoftvente ;

  }
  
  

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }



  postContact(ReservationHotel:ReservationHotel){
    return this.httpClient.post<ReservationHotel>(AjoutReservationUrl,ReservationHotel);
  
  }

  deleteHotel(nom:string){
    return this.httpClient.delete<Hotels>(DeleteHotelUrl+nom)
  }


}


