import { TestBed } from '@angular/core/testing';

import { SoireesService } from './soirees.service';

describe('SoireesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoireesService = TestBed.get(SoireesService);
    expect(service).toBeTruthy();
  });
});
