import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Soiree } from '../Models/soiree';
import { formatDate } from '@angular/common';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');

@Injectable({
  providedIn: 'root'
})
export class SoireesService {
  
  constructor(private httpClient:HttpClient) {}

  getSoiree():Observable<Soiree>{
    
    return this.httpClient.get<Soiree>(url,httpOptions);
  }
}
