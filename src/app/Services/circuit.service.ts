import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Circuit } from '../Models/circuit';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const url = "https://freedomtravel.tn/json/listCircuits.php";


@Injectable({
  providedIn: 'root'
})
export class CircuitService {
  
  constructor(private httpClient:HttpClient) {}
  
  getCircuit(circuit:Circuit):Observable<Circuit>{
    
    return this.httpClient.get<Circuit>(url,httpOptions);
  }
}
