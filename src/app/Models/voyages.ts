export class Voyages {
    
    id : number;
    nom : String;
    pays : String;
    titre : String;
    prix : String;
    nbrejour : number;
    nbrenuitee : number;
    departvoyages:Departvoyages;
}
class Departvoyages{
    datedepart:string;
    dateretour:string;
}
