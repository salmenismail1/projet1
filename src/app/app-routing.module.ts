import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { LoginComponent } from './Components/login/login.component';
import { ContactComponent } from './Components/contact/contact.component';
import { HotelsComponent } from './Components/hotels/hotels.component';
import { VoyageComponent } from './Components/voyage/voyage.component';
import { CircuitComponent } from './Components/circuit/circuit.component';
import { VreservationComponent } from './Components/vreservation/vreservation.component';
import { HreservationComponent } from './Components/hreservation/hreservation.component';
import { OmraComponent } from './Components/omra/omra.component';
import { SoireeComponent } from './Components/soiree/soiree.component';
import { DashBoardComponent } from './Components/Admin/dash-board/dash-board.component';

import { HotelComponent } from './Components/Admin/hotel/hotel.component';

const routes: Routes = [
  {path :'hotels',component: HotelsComponent},
  {path :'contact',component: ContactComponent},
  {path :'home',component: HomeComponent},
  {path :'circuit',component: CircuitComponent},
  {path :'login',component:LoginComponent},
  {path :'voyage',component:VoyageComponent},
  {path :'omra',component:OmraComponent},
  {path :'soiree',component:SoireeComponent},
  {path :'voyage',component:VoyageComponent},
  {path :'ReservationHotel/:id',component:HreservationComponent},
  {path :'ReservationVoyage/:id',component:VreservationComponent},
  {path :'Dashboard',component:DashBoardComponent},
  {path :'Dashboard/Hotels',component:HotelComponent},


  /*{path :'client',component:ClientComponent,canActivate: [AuthGuard]},*/
  /*{path :'billets',component:VenteBilletsComponent},*/
  {path :'**',component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
