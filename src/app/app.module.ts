import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { FooterComponent } from './Components/footer/footer.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HomeComponent } from './Components/home/home.component';
import { ContactComponent } from './Components/contact/contact.component';
import { LoginComponent as ModalComponent } from './Components/login/login.component';
import { VoyageComponent } from './Components/voyage/voyage.component';
import { HotelsComponent } from './Components/hotels/hotels.component';
import {HttpClientModule} from '@angular/common/http';
import { CalendarModule, DatePickerModule, TimePickerModule, DateRangePickerModule, DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule } from '@angular/forms';
import { CircuitComponent } from './Components/circuit/circuit.component';
import { OmraComponent } from './Components/omra/omra.component';
import { HreservationComponent } from './Components/hreservation/hreservation.component';
import { VreservationComponent } from './Components/vreservation/vreservation.component';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule,MatSidenavModule,MatInputModule,MatDividerModule,MatListModule, MatTableModule,MatToolbarModule, MatMenuModule,MatIconModule,MatCheckboxModule,MatTabsModule, MatProgressSpinnerModule} from '@angular/material';
import { FiltersPipe } from './filters.pipe';
import { FiltersearshPipe } from './filtersearsh.pipe';
import { FilteretoilePipe } from './filteretoile.pipe';
import { FilterprixPipe } from './filterprix.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SoireeComponent } from './Components/soiree/soiree.component';
import { SafeHtmlPipe } from './safe-html.pipe';
import { DureeVoyagePipe } from './duree-voyage.pipe';
import { SideBarComponent } from './Components/Admin/side-bar/side-bar.component';
import { DashBoardComponent } from './Components/Admin/dash-board/dash-board.component';
import { HotelComponent } from './Components/Admin/hotel/hotel.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    ContactComponent,
    ModalComponent,
    VoyageComponent,
    HotelsComponent,
    CircuitComponent,
    OmraComponent,
    HreservationComponent,		
    VreservationComponent, FiltersPipe, FiltersearshPipe, FilteretoilePipe, FilterprixPipe, SoireeComponent, SafeHtmlPipe, DureeVoyagePipe, SideBarComponent, DashBoardComponent, HotelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    CalendarModule, DatePickerModule, TimePickerModule, DateRangePickerModule, DateTimePickerModule,BrowserAnimationsModule,
    MatButtonModule, MatTabsModule,
    MatCheckboxModule,
    MatDialogModule,MatCardModule,MatInputModule, MatTableModule,
    MatToolbarModule,MatSidenavModule, MatMenuModule,MatIconModule,MatDividerModule,MatListModule, MatProgressSpinnerModule,NgbModule
    
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]
})
export class AppModule { }