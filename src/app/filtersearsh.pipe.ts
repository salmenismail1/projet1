import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtersearsh'
})
export class FiltersearshPipe implements PipeTransform {

  transform(ListeHotels: any, text:string): any {
    if( text === undefined) return ListeHotels;
      return ListeHotels.filter(function (hotel) {return hotel.nom.toLowerCase().includes(text.toLocaleLowerCase());})
    
  }



}
