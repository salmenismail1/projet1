import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filteretoile'
})
export class FilteretoilePipe implements PipeTransform {

  transform(ListeHotels: any, text:string): any {
    if( text === undefined) return ListeHotels;
      return ListeHotels.filter(function (hotel) {return hotel.categorie.toLowerCase().includes(text.toLocaleLowerCase());})
    
      
  }

}
