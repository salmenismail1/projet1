import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'dureeVoyage'
})
export class DureeVoyagePipe implements PipeTransform {

  transform(voyage: any, text:string): any {
    if( text === undefined) return null;
      return voyage.filter(function (v) {return (formatDate(v.datedepart, 'yyyy/MM/dd', 'en')+" vers "+formatDate(v.dateretour, 'yyyy/MM/dd', 'en')).toLowerCase().includes(text.toLocaleLowerCase());})
    
  }

}
