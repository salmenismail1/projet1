import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filters'
})
export class FiltersPipe implements PipeTransform {

  transform(ListeHotels: any, text:string): any {
    if( text === undefined || text ==="Liste des Villes") return ListeHotels;
      return ListeHotels.filter(function (hotel) {return hotel.ville.toLowerCase().includes(text.toLocaleLowerCase());})
    
      
  }

}
